# End-to-end tests of Mobicoop platform V3

E2E tests are written using the [Playwright](https://playwright.dev/) framework.

## Setup

### Basic scenario : playwright running locally

1. Install the dependencies and the web drivers

   ```sh
   npm install
   npx playwright install --with-deps
   ```

2. start the tested app stack. See [conductor](../conductor/README.md)

3. run the npm scripts from [./package.json](./package.json)

#### Overriding the targeted app URL

`URL_TESTED_FRONTEND` defaults to `http://localhost:8003`. See [./playwright.config.ts](./playwright.config.ts).

You can override it directly from terminal :

```sh
URL_TESTED_FRONTEND="http://some_url:some_port" npm run tests:ui
```

### Docker scenario : playwright as a server running in docker

#### Prerequisite : set env vars

```sh
# you need to copy the sample file
cp .env.sample .env
# it should work as it is, but you may want to edit its variables at your needs
```

#### Run

1. start the docker

   ```sh
   docker compose up -d
   # or in a wayland environment
   docker compose -f docker-compose.wayland.yaml up -d
   ```

2. start the tested app stack. You **should** start v3-front-mobile with the [/front/mobile/docker-compose.e2e.yml](../front/mobile/docker-compose.e2e.yml)

3. run the npm scripts from [./package.json](./package.json)

## Configuration for CI

To run the tests targeting another instance, set the URL in an environment variable:

```sh
export URL_TESTED_FRONTEND=https//v3.test.mobicoop.io
```

## Use Playwright recorder

1. it requires the front to be reachable on `localhost`, so you should start the frontend at `http://localhost:8003` (`http://v3-front-mobile-for-e2e:8004` won't work)
2. run `npm run codegen`

## OS specific setups

### Archlinux & Wayland

- in the `.env` file, uncomment the `CUSTOM_LAUNCH_OPTIONS_ARGS` variable
- use the [docker-compose.wayland.yaml](./docker-compose.wayland.yaml) (instead of the default `docker-compose.yaml`) :

  ```sh
  docker compose -f docker-compose.wayland.yaml up -d
  ```
