interface User {
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  password: string;
}

interface PunctualAd {
  comment: string;
  departureDateDayOfMonth: number;
  departureTime: string;
  destinationWaypoint: string;
  originWaypoint: string;
  role: string;
}

interface RecurrentAd {
  comment: string;
  destinationWaypoint: string;
  originWaypoint: string;
  role: string;
  schedule: {};
}

type Ad = PunctualAd | RecurrentAd;

interface UserAds {
  ads: Ad[];
}

interface Fixtures {
  registeredDriver: User & UserAds;
  loggedInUser: User & UserAds;
}
