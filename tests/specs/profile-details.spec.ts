import { expect, test } from "../fixtures";
import { HomePage } from "../page-objects/home.page";
import { LoginModal } from "../page-objects/login.modal";
import { ProfileDetailsPage } from "../page-objects/profile-details.page";

test.describe("Profile details page", () => {
  test("should allow deleting the account", async ({
    page,
    registeredDriver,
  }) => {
    const profileDetailsPage = new ProfileDetailsPage(page);
    await profileDetailsPage.open();

    const loginModal = new LoginModal(page);
    await expect(loginModal.modal).toBeVisible();
    await loginModal.login(registeredDriver);

    await profileDetailsPage.deleteButton.click();
    await expect(profileDetailsPage.confirmDialog).toBeVisible();
    await profileDetailsPage.confirmDeleteButton.click();

    const homePage = new HomePage(page);
    await expect(homePage.loginButton).toBeVisible();
    await homePage.loginButton.click();
    await loginModal.login(registeredDriver);
    await expect(
      page
        .getByText("verify your email and password", { exact: false })
        .first(),
    ).toBeVisible();
  });
});
