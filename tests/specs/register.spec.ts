import { expect, test } from "../fixtures";
import { HomePage } from "../page-objects/home.page";
import { LoginModal } from "../page-objects/login.modal";
import { RegisterPage } from "../page-objects/register.page";

test.describe("Registration", () => {
  test("should be possible with a valid user", async ({
    page,
    registeredDriver,
  }) => {
    const registerPage = new RegisterPage(page);
    const loginModal = new LoginModal(page);
    await registerPage.open();

    await registerPage.personalDataAccordion.click();
    await registerPage.firstNameInput.fill(registeredDriver.firstName);
    await registerPage.lastNameInput.fill(registeredDriver.lastName);
    await registerPage.phoneInput.fill(registeredDriver.phone);
    await registerPage.passwordInput.fill(registeredDriver.password);

    await registerPage.genderAccordion.click();
    await registerPage.genderSelect("female").click();

    await registerPage.emailAccordion.click();
    await registerPage.emailInput.fill(registeredDriver.email);

    await registerPage.conditionsCheckbox.click();
    await registerPage.registerButton.click();

    await registerPage.verificationCodeInput.fill("424242");
    await registerPage.registerButton.click();

    //it should allow logging in right after registration"
    await expect(loginModal.modal).toBeVisible();
    await loginModal.login(registeredDriver);

    //We should land on home page
    expect(new URL(page.url()).pathname).toBe(HomePage.path);
    const homePage = new HomePage(page);
    await expect(homePage.profileButton).toBeVisible();
  });
});
