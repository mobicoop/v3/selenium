import { defaultPunctualAd } from "../data/ads";
import { expect, test } from "../fixtures";
import { AdFormSummary } from "../page-objects/ad-form-summary.page";
import { AdForm } from "../page-objects/ad-form.page";
import { AdCard } from "../page-objects/ad.card";
import { AddressSearchModal } from "../page-objects/address-search.modal";
import { DatePicker } from "../page-objects/date-picker";
import { HomePage } from "../page-objects/home.page";
import { IonToggler } from "../page-objects/ion-toggler";
import { TimePicker } from "../page-objects/time-picker";
import { TripsPage } from "../page-objects/trips.page";

test.describe("Ad publication", () => {
  test("should be able to publish a punctual ad as a logged in user", async ({
    page,
    loggedInUser,
  }) => {
    const homePage = new HomePage(page);

    await expect(homePage.myAds.publishButton).toBeVisible();
    await homePage.myAds.publishButton.click();

    const form = new AdForm(page);

    const {
      comment,
      departureDateDayOfMonth,
      departureTime,
      destinationWaypoint,
      originWaypoint,
      role,
    } = defaultPunctualAd;

    // slide waypoints
    expect(form.nextButton).toHaveAttribute("disabled");
    const addressSearchModal = new AddressSearchModal(page);
    await expect(form.departureWaypointInput).toBeVisible();
    await form.departureWaypointInput.click();
    await addressSearchModal.searchInput.fill(originWaypoint);
    await expect(addressSearchModal.firstResult).toBeVisible();
    await addressSearchModal.firstResult.click();

    await expect(form.destinationWaypointInput).toBeVisible();
    await form.destinationWaypointInput.click();
    await addressSearchModal.searchInput.fill(destinationWaypoint);
    await expect(addressSearchModal.firstResult).toContainText(
      destinationWaypoint,
    );
    await addressSearchModal.firstResult.click();
    expect(form.nextButton).not.toHaveAttribute("disabled");
    await form.nextButton.click();

    // slide role
    expect(form.nextButton).toHaveAttribute("disabled");
    await expect(form.roleInput(role)).toBeVisible();
    await form.roleInput(role).click();
    expect(form.nextButton).not.toHaveAttribute("disabled");
    await form.nextButton.click();

    // slide frequency
    expect(form.nextButton).toHaveAttribute("disabled");
    const ionToggler = new IonToggler(page, "recurrentFrequencyToggle");
    await expect(ionToggler.label("Recurrent trip")).toBeVisible();
    await expect(ionToggler.inputSwitch).toHaveAttribute(
      "aria-checked",
      "true",
    );
    await ionToggler.nativeWrapperCheckbox.uncheck();
    await expect(ionToggler.inputSwitch).toHaveAttribute(
      "aria-checked",
      "false",
    );
    expect(form.nextButton).not.toHaveAttribute("disabled");
    await form.nextButton.click();

    // slide planning
    // date picker
    expect(form.nextButton).toHaveAttribute("disabled");
    const datePicker = new DatePicker(page);
    await form.departureDateInput.click();
    await datePicker.pickDateNextMonth(departureDateDayOfMonth);
    const date = new Date(await form.departureDateInput.inputValue());
    expect(date.getDate()).toBe(departureDateDayOfMonth);
    // time picker
    const timePicker = new TimePicker(page);
    await form.departureTimeInput.click();
    const [hour, minutes] = departureTime.split(":");
    await timePicker.pickHour(hour);
    await timePicker.pickMinutes(minutes);
    await timePicker.validateButton.click();
    expect(await form.departureTimeInput.inputValue()).toContain(departureTime);
    expect(form.nextButton).not.toHaveAttribute("disabled");
    await form.nextButton.click();

    // slide comment
    expect(form.nextButton).not.toHaveAttribute("disabled");
    await expect(form.commentInput).toBeVisible();
    await form.commentInput.fill(comment ?? "");
    await form.nextButton.click();

    // slide summary
    await expect(form.publishButton).toBeVisible();
    expect(form.publishButton).toHaveAttribute("disabled");

    const adFormSummary = new AdFormSummary(page);
    await expect(adFormSummary.title).toBeVisible();
    const adCard = new AdCard(
      page,
      loggedInUser,
      originWaypoint,
      destinationWaypoint,
      date,
      departureTime,
    );
    await expect(adCard.card).toBeVisible();
    await expect(adCard.cardHeaderAvatar).toBeVisible();
    await expect(adCard.cardHeaderUserInfosName).toBeVisible();
    await expect(adCard.cardHeaderUserInfoRole).toHaveText("Driver");
    await expect(adCard.cardHeaderDate).toBeVisible();
    await expect(adCard.cardContent).toBeVisible();
    await expect(adCard.cardContentJourneyStepPickup).toBeVisible();
    await expect(adCard.cardContentJourneyStepDropoff).toBeVisible();

    await expect(adFormSummary.comment).toBeVisible();
    await expect(adFormSummary.comment).toHaveText(comment);

    await expect(form.hasDrivingLicenceCheckbox).toBeVisible();
    await expect(
      form.hasDrivingLicenceCheckbox.locator(".checkbox-wrapper"),
    ).not.toBeChecked();
    await form.hasDrivingLicenceCheckbox.click();
    await expect(
      form.hasDrivingLicenceCheckbox.locator(".checkbox-wrapper"),
    ).toBeChecked();
    expect(form.publishButton).not.toHaveAttribute("disabled");
    await form.publishButton.click();

    const tripsPage = new TripsPage(page);
    await expect(tripsPage.trips.first()).toBeVisible();
  });
});
