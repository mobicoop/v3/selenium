import { expect } from "@playwright/test";
import { defaultPunctualAd } from "../data/ads";
import { jane } from "../data/users";
import { test } from "../fixtures";
import { AdCard } from "../page-objects/ad.card";
import { AddressSearchModal } from "../page-objects/address-search.modal";
import { EditSearchBottomSheet } from "../page-objects/edit-search.bottom-sheet";
import { HomePage } from "../page-objects/home.page";
import { ResultsPage } from "../page-objects/results.page";

test.describe("Search", () => {
  test("should find the published ad as a logged in user", async ({
    page,
    loggedInUser,
  }) => {
    const homePage = new HomePage(page);
    // verify user is logged in
    await expect(homePage.profileButton).toBeVisible();
    expect(homePage.profileButton).toContainText(loggedInUser.firstName);

    const { originWaypoint, destinationWaypoint, departureDateDayOfMonth } =
      defaultPunctualAd;
    await homePage.searchForm.fill(
      originWaypoint,
      destinationWaypoint,
      departureDateDayOfMonth,
    );
    await homePage.searchForm.searchButton.click();

    const resultsPage = new ResultsPage(page);
    await expect(resultsPage.header).toBeVisible();
    expect(new URL(page.url()).pathname).toBe(ResultsPage.path);
    await expect(resultsPage.summary).toBeVisible();
    await expect(resultsPage.summaryOrigin).toContainText(originWaypoint);
    await expect(resultsPage.summaryDestination).toContainText(
      destinationWaypoint,
    );
    const adCard = new AdCard(page, jane, originWaypoint, destinationWaypoint);
    await expect(adCard.card).toBeVisible();
    await expect(adCard.cardHeaderAvatar).toBeVisible();
    await expect(adCard.cardHeaderUserInfosName).toHaveText("Jane D.");
    await expect(adCard.cardContent).toBeVisible();
    await expect(adCard.cardContentJourneyStepPickup).toBeVisible();
    await expect(adCard.cardContentJourneyStepDropoff).toBeVisible();

    // Modify search
    await resultsPage.summary.click();
    const editSearch = new EditSearchBottomSheet(page);
    await expect(editSearch.bottomSheet).toBeVisible();
    await expect(editSearch.originInputField).toHaveValue(
      "Nancy, 54000, France",
    );
    await expect(editSearch.destinationInputField).toHaveValue(
      "Metz, 57000, France",
    );
    await editSearch.swapButton.click();
    await expect(editSearch.originInputField).toHaveValue(
      "Metz, 57000, France",
    );
    await expect(editSearch.destinationInputField).toHaveValue(
      "Nancy, 54000, France",
    );

    const addressSearchModal = new AddressSearchModal(page);

    await editSearch.originInputField.click();
    await editSearch.originInputField.fill("Grenoble");
    await expect(
      addressSearchModal.firstResult.getByText("Grenoble"),
    ).toBeVisible();
    await addressSearchModal.firstResult.click();

    await editSearch.destinationInputField.click();
    await editSearch.destinationInputField.fill("Valence");
    await expect(
      addressSearchModal.firstResult.getByText("Valence"),
    ).toBeVisible();
    await addressSearchModal.firstResult.click();

    await expect(editSearch.originInputField).toHaveValue(
      "Grenoble, 38000, France",
    );
    await expect(editSearch.destinationInputField).toHaveValue(
      "Valence, 26000, France",
    );
    await editSearch.submitSearchButton.click();
    await expect(editSearch.bottomSheet).not.toBeVisible();
    await expect(resultsPage.summary).toBeVisible();
    await expect(resultsPage.summaryOrigin).toHaveText("Departure : Grenoble");
    await expect(resultsPage.summaryDestination).toHaveText(
      "Arrival : Valence",
    );
  });
});
