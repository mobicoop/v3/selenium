import { test as base } from "@playwright/test";
import { defaultPunctualAd } from "./data/ads";
import { jane } from "./data/users";
import { HomePage } from "./page-objects/home.page";
import { LoginModal } from "./page-objects/login.modal";

export const test = base.extend<Fixtures>({
  //Will be registered by the registration test and ads created by the ads test
  registeredDriver: async ({}, use) => {
    await use({ ...jane, ads: [defaultPunctualAd] });
  },
  loggedInUser: async ({ page, registeredDriver }, use) => {
    const homePage = new HomePage(page);
    await homePage.open();
    await homePage.loginButton.click();
    const loginModal = new LoginModal(page);
    await loginModal.login(registeredDriver);
    await use(registeredDriver);
  },
});

export { expect } from "@playwright/test";
