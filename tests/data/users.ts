export const jane: User = {
  firstName: "Jane",
  lastName: "Doe",
  email: "jane.doe@email.com",
  phone: "+33611001100",
  password: "Dummy_passw0rd",
};

export const john: User = {
  firstName: "John",
  lastName: "Doe",
  email: "john.doe@email.com",
  phone: "+33622002200",
  password: "Dummy_passw0rd",
};

export const jim: User = {
  firstName: "Jim",
  lastName: "Doe",
  email: "jim.doe@email.com",
  phone: "+33633003300",
  password: "Dummy_passw0rd",
};
