// To make date selection easier and always in the future,
// ad fixtures specify the day of next month
export const defaultPunctualAd: PunctualAd = {
  comment: "Lorem ipsum dolor sit amet",
  departureDateDayOfMonth: 5,
  departureTime: "11:55",
  destinationWaypoint: "Metz",
  originWaypoint: "Nancy",
  role: "I drive",
};
