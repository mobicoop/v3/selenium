import { Locator, Page } from "@playwright/test";
import { PageObject } from "./page-object";

export class IonToggler extends PageObject {
  toggler: Locator;

  constructor(
    readonly page: Page,
    id: string,
  ) {
    super(page);
    this.toggler = this.page.locator(`ion-toggle#${id}`);
  }

  label(hasText: string) {
    return this.toggler.locator("ion-label").filter({ hasText });
  }

  get inputSwitch() {
    return this.toggler.getByRole("switch");
  }

  get nativeWrapperCheckbox() {
    return this.toggler.locator(".native-wrapper");
  }
}
