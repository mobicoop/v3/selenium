import { PageObject } from "./page-object";

export class AdForm extends PageObject {
  get swiper() {
    return this.page
      .locator(".ion-page")
      .filter({ has: this.page.locator("swiper-container swiper-slide") });
  }

  get nextButton() {
    return this.swiper.locator("ion-button").filter({ hasText: "Next" });
  }

  get departureWaypointInput() {
    return this.swiper.getByLabel("Departure address", { exact: true });
  }
  get destinationWaypointInput() {
    return this.swiper.getByLabel("Destination address", { exact: true });
  }

  roleInput(role: string) {
    return this.swiper.getByRole("radio", { name: role });
  }

  get departureDateInput() {
    return this.swiper.getByLabel("Departure date");
  }

  get departureTimeInput() {
    return this.swiper.getByLabel("Departure time");
  }

  get commentInput() {
    return this.swiper.getByPlaceholder("Add a comment to ad");
  }

  get hasDrivingLicenceCheckbox() {
    return this.swiper.locator("ion-checkbox");
  }

  get publishButton() {
    return this.swiper.getByRole("button", { name: "Publish" });
  }
}
