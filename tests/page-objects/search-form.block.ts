import { expect, Locator } from "@playwright/test";
import { AddressSearchModal } from "./address-search.modal";
import { DatePicker } from "./date-picker";
import { PageObject } from "./page-object";

export class SearchForm extends PageObject {
  constructor(public readonly locator: Locator) {
    super(locator.page());
  }

  get originWaypointInput() {
    return this.locator.getByPlaceholder("Departure", { exact: true });
  }
  get destinationWaypointInput() {
    return this.locator.getByPlaceholder("Arrival");
  }
  get departureDateInput() {
    return this.locator.getByPlaceholder("Departure date");
  }

  get searchButton() {
    return this.locator.getByRole("button", { name: "Search" });
  }

  async fill(origin: string, destination: string, dayOfMonth: number) {
    const searchModal = new AddressSearchModal(this.page);
    const datePicker = new DatePicker(this.page);

    await this.originWaypointInput.click();
    await searchModal.searchInput.fill(origin);
    await expect(searchModal.firstResult).toBeVisible();
    await searchModal.firstResult.click();

    await this.destinationWaypointInput.click();
    await searchModal.searchInput.fill(destination);
    await expect(searchModal.firstResult).toContainText(destination);
    await searchModal.firstResult.click();

    await this.departureDateInput.click();
    await datePicker.pickDateNextMonth(dayOfMonth);
  }
}
