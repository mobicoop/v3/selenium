import { PageObject } from "./page-object";

export class ProfileDetailsPage extends PageObject {
  async open() {
    return this.page.goto("/tabs/profile/details");
  }

  get deleteButton() {
    return this.page.locator("ion-button:has-text('Delete account')");
  }

  get confirmDialog() {
    return this.page.locator("ion-modal:visible");
  }
  get confirmDeleteButton() {
    return this.confirmDialog.locator(
      "section.action-sheet-content ion-button:has-text('Delete')",
    );
  }
}
