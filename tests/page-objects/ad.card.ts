import { Locator, Page } from "@playwright/test";
import { PageObject } from "./page-object";

export class AdCard extends PageObject {
  card: Locator;
  userFullName: string;

  constructor(
    readonly page: Page,
    public user: User,
    public originWaypoint: string,
    public destinationWaypoint: string,
    public date?: Date,
    public departureTime?: string,
  ) {
    super(page);
    this.userFullName = `${user.firstName} ${user.lastName.charAt(0)}`;
    this.card = this.page.locator("section ion-card", {
      hasText: this.userFullName,
    });
  }

  // HEADER
  get cardHeader() {
    return this.card.locator("ion-card-header");
  }

  get cardHeaderAvatar() {
    return this.cardHeader.getByAltText(this.userFullName);
  }

  get cardHeaderUserInfosName() {
    return this.cardHeader
      .locator(".user-info ion-text")
      .getByText(this.userFullName);
  }

  get cardHeaderUserInfoRole() {
    return this.cardHeader.locator(".user-info ion-label");
  }

  get cardHeaderDate() {
    const dateToFound = this.date
      ? `${this.date.toLocaleString("en", { weekday: "short" })}, ${
          this.date.getMonth() + 1
        }.${this.date.getDate()}`
      : ".";

    return this.cardHeader
      .locator(".planning .departure-date")
      .getByText(dateToFound);
  }

  // CONTENT
  get cardContent() {
    return this.card.locator("ion-card-content", {
      hasText: this.originWaypoint,
    });
  }

  get cardContentJourneyStepPickup() {
    return this.cardContent
      .locator("ion-row")
      .getByText(this.originWaypoint)
      .nth(0);
  }

  get cardContentJourneyStepDropoff() {
    return this.cardContent
      .locator("ion-row")
      .getByText(this.destinationWaypoint)
      .nth(0);
  }
}
