import { PageObject } from "./page-object";

export class RegisterPage extends PageObject {
  async open() {
    return this.page.goto("/register");
  }

  // Personal data
  get personalDataAccordion() {
    return this.page
      .locator("ion-accordion")
      .getByRole("heading", { name: "Personal data" });
  }
  get firstNameInput() {
    return this.page.locator("ion-input#firstName input");
  }
  get lastNameInput() {
    return this.page.locator("ion-input#lastName input");
  }
  get phoneInput() {
    return this.page.locator("ion-input#phone input");
  }
  get passwordInput() {
    return this.page.locator("ion-input#password input");
  }

  // Gender
  get genderAccordion() {
    return this.page
      .locator("ion-accordion")
      .getByRole("heading", { name: "Gender" });
  }
  genderSelect(gender: string) {
    return this.page.locator("ion-radio-group#gender").getByText(gender);
  }

  // Email
  get emailAccordion() {
    return this.page
      .locator("ion-accordion")
      .getByRole("heading", { name: "Email" });
  }
  get emailInput() {
    return this.page.locator("ion-input#email input");
  }

  get conditionsCheckbox() {
    return this.page.locator("ion-checkbox#agree");
  }

  get registerButton() {
    return this.page.locator("ion-button:has-text('Register'):visible");
  }

  get verificationCodeInput() {
    return this.page.getByPlaceholder("Enter validation code received by SMS");
  }
}
