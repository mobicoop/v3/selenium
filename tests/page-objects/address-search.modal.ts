import { PageObject } from "./page-object";

export class AddressSearchModal extends PageObject {
  get searchInput() {
    return this.page.locator("ion-input#addressSearch input");
  }

  get firstResult() {
    return this.page.getByTestId("result_0");
  }
}
