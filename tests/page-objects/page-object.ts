import { Page } from "@playwright/test";

export class PageObject {
  constructor(readonly page: Page) {}
}
