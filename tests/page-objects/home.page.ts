import { MyAds } from "./my-ads.block";
import { PageObject } from "./page-object";
import { SearchForm } from "./search-form.block";

export class HomePage extends PageObject {
  static path = "/tabs/search";

  async open() {
    return this.page.goto(HomePage.path);
  }

  get myAds() {
    return new MyAds(
      this.page.locator("ion-accordion").filter({ hasText: "Your ads" }),
    );
  }

  get searchForm() {
    return new SearchForm(
      this.page.locator("ion-content", { hasText: "Where are you going" }),
    );
  }

  get welcomeFooter() {
    return this.page.locator("ion-footer");
  }
  get standardFooter() {
    return this.page.locator("ion-tab-bar");
  }

  get registerButton() {
    return this.welcomeFooter.locator("ion-button:has-text('Register')");
  }
  get loginButton() {
    return this.welcomeFooter.locator("ion-button:has-text('Login')");
  }

  get profileButton() {
    return this.standardFooter.locator("#tab-button-profile");
  }
}
