import { PageObject } from "./page-object";

export class ResultsPage extends PageObject {
  static path = "/tabs/search/results";

  get header() {
    return this.page.locator("ion-header ion-title").getByText("Results");
  }

  get summary() {
    return this.page.locator("ion-card ion-card-content", {
      hasText: "Departure",
    });
  }

  get summaryOrigin() {
    return this.summary.locator("p", { hasText: "Departure" });
  }

  get summaryDestination() {
    return this.summary.locator("p", { hasText: "Arrival" });
  }
}
