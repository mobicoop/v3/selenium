import { ResultsPage } from "./results.page";

export class EditSearchBottomSheet extends ResultsPage {
  get bottomSheet() {
    return this.page
      .locator("ion-modal .ion-page ion-content")
      .getByRole("heading", { name: "Where are you going" });
  }

  get originInputField() {
    return this.page.getByRole("textbox", { name: "Departure" });
  }

  get destinationInputField() {
    return this.page.getByRole("textbox", { name: "Arrival" });
  }

  get swapButton() {
    return this.page.locator("section #swap-btn");
  }

  get submitSearchButton() {
    return this.page.getByRole("button", { name: "Search" });
  }
}
