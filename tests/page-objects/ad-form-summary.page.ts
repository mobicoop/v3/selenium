import { AdForm } from "./ad-form.page";

export class AdFormSummary extends AdForm {
  get title() {
    return this.swiper.getByText("Summary");
  }

  get comment() {
    return this.swiper.locator("ion-card ion-item ion-label p");
  }
}
