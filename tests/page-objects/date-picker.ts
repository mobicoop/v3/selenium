import { PageObject } from "./page-object";

export class DatePicker extends PageObject {
  get today() {
    return this.page.locator(".calendar-day-today");
  }

  async selectDate(date: Date) {
    const monthName = date.toLocaleString("en", { month: "long" });
    return this.page
      .getByRole("button", {
        disabled: false,
        includeHidden: false,
        //We select by month name substring to make sure that we don't pick
        //the day of another month while trhe next-month transition is going on
        name: monthName,
      })
      .getByText(date.getDate().toString(), { exact: true })
      .click();
  }

  get validateButton() {
    return this.page.getByRole("button", { name: "Validate" });
  }

  async pickDateNextMonth(dayOfMonth: number) {
    await this.page.getByLabel("Next month").click();
    const date = new Date();
    date.setMonth(date.getMonth() + 1);
    date.setDate(dayOfMonth);
    await this.selectDate(date);
    return this.validateButton.click();
  }
}
