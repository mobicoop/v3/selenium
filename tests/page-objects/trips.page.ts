import { PageObject } from "./page-object";

export class TripsPage extends PageObject {
  static path = "/tabs/trips";

  get trips() {
    return this.page.locator("#COMING").locator("ion-card");
  }
}
