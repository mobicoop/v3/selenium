import { PageObject } from "./page-object";

export class LoginModal extends PageObject {
  get modal() {
    return this.page.locator("ion-modal:visible");
  }

  get usernameInput() {
    return this.modal.locator("ion-input#username input");
  }
  get passwordInput() {
    return this.modal.locator("ion-input#password input");
  }

  get loginButton() {
    return this.modal.locator("ion-button#login");
  }

  async login(user: { email: string; password: string }) {
    await this.usernameInput.fill(user.email);
    await this.passwordInput.fill(user.password);
    return this.loginButton.click();
  }
}
