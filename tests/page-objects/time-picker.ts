import { PageObject } from "./page-object";

export class TimePicker extends PageObject {
  get picker() {
    return this.page.locator("ion-datetime ion-picker");
  }

  pickHour(hour: string) {
    return this.picker.getByText(hour).click();
  }
  pickMinutes(minutes: string) {
    return this.picker.getByText(minutes).click();
  }

  get validateButton() {
    return this.page.getByRole("button", { name: "Validate" });
  }
}
