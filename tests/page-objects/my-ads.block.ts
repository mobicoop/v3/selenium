import { Locator } from "@playwright/test";
import { PageObject } from "./page-object";

export class MyAds extends PageObject {
  constructor(public readonly locator: Locator) {
    super(locator.page());
  }

  get displayedAds() {
    return this.locator.locator("ion-item-sliding");
  }

  get publishButton() {
    return this.locator.locator("ion-button:has-text('Publish')");
  }
}
