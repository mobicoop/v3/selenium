import { defineConfig, devices } from "@playwright/test";

// https://playwright.dev/docs/test-configuration.
export default defineConfig({
  testDir: "./tests",
  fullyParallel: true,
  forbidOnly: !!process.env.CI,
  retries: process.env.CI ? 2 : 0,
  workers: process.env.CI ? 1 : undefined,
  reporter: "list",
  use: {
    baseURL: process.env.URL_TESTED_FRONTEND || "http://localhost:8003",
    trace: "on-first-retry",
    launchOptions: { args: [process.env.CUSTOM_LAUNCH_OPTIONS_ARG || ""] },
  },
  projects: [
    {
      name: "registration",
      testMatch: "register.spec.ts",
      teardown: "profile details",
      use: { ...devices["Pixel 5"] },
    },
    {
      name: "ad form",
      testMatch: "ad-form.spec.ts",
      dependencies: ["registration"],
      use: { ...devices["Pixel 5"] },
    },
    {
      name: "search",
      testMatch: "search.spec.ts",
      dependencies: ["ad form"],
      use: { ...devices["Pixel 5"] },
    },
    {
      name: "profile details",
      testMatch: "profile-details.spec.ts",
      use: { ...devices["Pixel 5"] },
    },
  ],
});
